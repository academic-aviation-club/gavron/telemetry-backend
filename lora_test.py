import serial
from telemetry_definitions.generated import definitions_pb2
from google.protobuf.message import DecodeError
from google.protobuf.json_format import MessageToDict

lora = serial.Serial(
    port='/dev/ttyUSB0',
    baudrate=115200
)

received = bytearray()

while True:
    try:
        received_byte = lora.read(1)
        # print(received_byte)
        # print(type(received_byte))
        received.append(received_byte[0])
        
        telem_packet = definitions_pb2.GavronTelemFrame.FromString(received)
        print(MessageToDict(telem_packet))
        received = bytearray()
    except KeyboardInterrupt:
        lora.close()
        exit(1)
    except DecodeError:
        print('Protobuf is not going brr')

    