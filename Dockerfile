FROM wnt3rmute/archlinux-with-goodies

RUN mkdir /telem_backend

COPY requirements.txt /telem_backend
WORKDIR /telem_backend
RUN pip install -r requirements.txt

COPY . /telem_backend

CMD python main.py