import asyncio
import logging

import websockets
from websockets import WebSocketServerProtocol

logger = logging.getLogger(__name__)


class WebsocketServer:
    def __init__(self):
        self.clients = set()
        self.message = ""

    async def register(self, websocket: WebSocketServerProtocol) -> None:
        self.clients.add(websocket)
        logger.info(f"{websocket.remote_address} connects.")

    async def unregister(self, ws: WebSocketServerProtocol) -> None:
        self.clients.remove(ws)
        logging.info(f"{ws.remote_address} disconnects")

    async def send_to_all_clients(self, message: str) -> None:
        if not any(self.clients):
            return
        await asyncio.wait([client.send(message) for client in self.clients])

    async def ws_handler(self, ws: WebSocketServerProtocol, uri: str) -> None:
        await self.register(ws)
        await ws.wait_closed()
        await self.unregister(ws)
        