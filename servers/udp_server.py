import asyncio
import logging

logger = logging.getLogger(__name__)


class UDPServer:
    def __init__(self, telemetry_packets_queue: asyncio.Queue):
        self.telemetry_packets_queue = telemetry_packets_queue

    def connection_made(self, transport):
        self.transport = transport

    def connection_lost(self, transport):
        logger.warning("UDP: connection lost (wtf?)")

    # UDP data received
    def datagram_received(self, data, addr):
        logger.debug(f"GOT UDP DATA: {data}")
        self.telemetry_packets_queue.put_nowait(data)
        