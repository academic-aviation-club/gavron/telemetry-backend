import asyncio
import logging
import time

import coloredlogs
import websockets

from argparse_get_connection import get_connection_string_from_args

coloredlogs.install(level="INFO")
logger = logging.getLogger(__name__)


async def consumer_handler(websocket):
    async for message in websocket:
        logger.info(message)


async def consume(connection: str):
    websockets.websocket_resource_url = f"ws://{connection}"
    async with websockets.connect(websockets.websocket_resource_url) as websocket:
        await consumer_handler(websocket)


if __name__ == "__main__":
    connection_string = get_connection_string_from_args(
        "Command line utility connecting to a websocket"
        "server and dumping all incoming data"
    )

    loop = asyncio.get_event_loop()
    loop.run_until_complete(consume(connection_string))
    loop.run_forever()
