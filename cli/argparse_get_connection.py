import argparse

def get_connection_string_from_args(app_name):
    """ Unified function for parsing the connection string

    Used across the util scripts

    :param app_name: select the app name to show when -h is used
    :type app_name: str
    """

    parser = argparse.ArgumentParser(
        description=app_name
    )
    parser.add_argument(
        "CONNECTION",
        help="Specify a connection in format HOSTNAME:PORT (e.g. localhost:2137)",
    )
    args = parser.parse_args()

    return args.CONNECTION
