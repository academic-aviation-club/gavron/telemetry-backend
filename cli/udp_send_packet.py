import socket

from argparse_get_connection import get_connection_string_from_args

bytes_to_send = b'\n\x0c\r\xf3\xe0\x01\x00\x10\x01\x1d\x05\x00\x00\x00\x12\x0f\r\x00\x00\xa0@\x15\x00\x00 A\x1d\x00\x00pA'

connection_string = get_connection_string_from_args(
    "Command line utility sending a UDP packet to the telemetry server"
)

addr, port = connection_string.split(":")
server_address_and_port = (addr, int(port))

# Create a UDP socket at client side
udp_client_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

# Send to server using created UDP socket
udp_client_socket.sendto(bytes_to_send, server_address_and_port)
