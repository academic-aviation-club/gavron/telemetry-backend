import asyncio
import json
import logging
import os

import coloredlogs
import websockets
from google import protobuf
from google.protobuf.json_format import MessageToDict

import config as CONFIG
from servers.udp_server import UDPServer
from servers.websocket_server import WebsocketServer
from telemetry_definitions.generated.definitions_pb2 import GavronTelemFrame
from utils.active_flights import ActiveFlightsHandler

POSSIBLE_LOGGING_LEVELS = ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]

colored_logs_installed = False
for logging_level in POSSIBLE_LOGGING_LEVELS:
    if os.environ.get(logging_level) != None:
        print(f"Logs level from env vars: {logging_level}")
        coloredlogs.install(level=logging_level)
        colored_logs_installed = True

if not colored_logs_installed:
    print("Using default logging level: DEBUG")
    coloredlogs.install(level="DEBUG")

logger = logging.getLogger(__name__)


async def main():
    logger.info("Starting the telemetry backend")

    telemetry_packets_queue = asyncio.Queue()

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    websocket_server = WebsocketServer()  # loop.run_until_complete(start_server)

    # One protocol instance will be created to serve all
    # client requests.

    logger.info(
        f"Starting the UDP server at {CONFIG.SERVER_IP}:{CONFIG.UDP_SERVER_PORT}"
    )
    transport, _protocol = await loop.create_datagram_endpoint(
        lambda: UDPServer(telemetry_packets_queue),
        local_addr=(CONFIG.SERVER_IP, CONFIG.UDP_SERVER_PORT),
    )

    logger.info(
        "Starting the Websocket server at "
        f"{CONFIG.SERVER_IP}:{CONFIG.WEBSOCKET_SERVER_PORT}"
    )
    await websockets.serve(
        websocket_server.ws_handler, CONFIG.SERVER_IP, CONFIG.WEBSOCKET_SERVER_PORT
    )

    active_flights = ActiveFlightsHandler()

    while True:

        logging.debug(f"Number of active flights: {active_flights.active_flights_num}")

        try:
            # Wait for a UDP telem packet
            raw_telem_packet = await asyncio.wait_for(
                telemetry_packets_queue.get(), timeout=2
            )

        except asyncio.TimeoutError:
            logging.debug("No telem received :(")
            active_flights.handle_telem_timeout()
            continue

        try:
            telem_packet = GavronTelemFrame()
            telem_packet.ParseFromString(raw_telem_packet)

            active_flights.handle_telem_packet(telem_packet)

            # Debug print the packet
            logging.info(MessageToDict(telem_packet))

            # Forward it to all Websocket listeners
            await websocket_server.send_to_all_clients(raw_telem_packet)
        except protobuf.message.DecodeError:
            logging.error("Protobuf decode error occured")

    transport.close()


if __name__ == "__main__":
    asyncio.run(main())
