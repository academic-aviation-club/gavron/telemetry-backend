import datetime
import json
import logging
import time
from typing import List

import requests
from google.protobuf.json_format import MessageToDict
from telemetry_definitions.generated.definitions_pb2 import GavronTelemFrame

logger = logging.getLogger(__name__)
GavronFlight = List[GavronTelemFrame]


class ActiveFlightsHandler:
    """Takes care of storing the currently active flights

    saves all the telemetry packets and sends it
    to the main backend server when a flight is over
    (or if no packets were received in FLIGHT_TIMEOUT_SECONDS)
    """

    FLIGHT_TIMEOUT_SECONDS = 10

    def __init__(self):
        self.active_flights = {}

    @property
    def active_flights_num(self):
        return len(self.active_flights)

    def belongs_to_any_of_currently_active_flights(self, packet: GavronTelemFrame):
        return (
            packet.header.machine_id,
            packet.header.flight_id,
        ) in self.active_flights

    def add_active_flight_for_packet(self, packet: GavronTelemFrame):
        logger.info(
            f"Adding new active flight: {packet.header.machine_id}, "
            f"{packet.header.flight_id}"
        )
        self.active_flights[packet.header.machine_id, packet.header.flight_id] = []

    def handle_telem_packet(self, packet: GavronTelemFrame):

        if not self.belongs_to_any_of_currently_active_flights(packet):
            logger.info("New active flight!")
            self.add_active_flight_for_packet(packet)
            logger.info(f"Num of active flights: {self.active_flights_num}")

        self.active_flights[packet.header.machine_id, packet.header.flight_id].append(
            packet
        )

    def get_last_timestamp_for_flight(self, flight: GavronFlight) -> int:
        return flight[-1].header.timestamp

    def flight_has_timed_out(self, flight: GavronFlight) -> bool:
        return (
            time.time() - self.get_last_timestamp_for_flight(flight)
            > ActiveFlightsHandler.FLIGHT_TIMEOUT_SECONDS
        )

    def remove_flight_from_active_flights(self, flight: GavronFlight):
        logger.info(
            f"Flight: {flight[-1].header.machine_id}, "
            f"{flight[-1].header.flight_id} is no longer active"
        )
        del self.active_flights[
            flight[-1].header.machine_id, flight[-1].header.flight_id
        ]

    def handle_telem_timeout(self):

        flights_to_end = []

        for _flight_id, flight in self.active_flights.items():
            if self.flight_has_timed_out(flight):
                flights_to_end.append(flight)

        if not any(flights_to_end):
            return

        for flight_to_end in flights_to_end:
            self.remove_flight_from_active_flights(flight_to_end)

        # TODO: send the flights to end to the server
        logger.info(f"Sending {len(flights_to_end)} flight[s] to archivisation")

        for flight_to_end in flights_to_end:
            archived_flight_data = [MessageToDict(packet) for packet in flight_to_end]
            requests.post(
                "http://vps-e584bd76.vps.ovh.net:8000/telemetry/",
                data={"telemetry": json.dumps(archived_flight_data)},
            )
            # TEMPORARY: backing up to localstorage
            logger.warning("TEMPORARY: Backing up to local storage as well")
            
            backup_file_name = (
                f"/archive/flight_{str(datetime.datetime.now())}.json".replace(" ", "_")
            )
            
            with open(backup_file_name, "w") as telem_backup:
                telem_backup.write(json.dumps(archived_flight_data))

        logger.info(f"Num of active flights: {self.active_flights_num}")
