import unittest
from .active_flights import ActiveFlightsHandler
from telemetry_definitions.generated.definitions_pb2 import GavronTelemFrame
import time


class ActiveFlightHandlerTests(unittest.TestCase):
    def setUp(self):
        self.active_flights_handler = ActiveFlightsHandler()

    def test_after_init_state(self):
        """Test if after initialisation,
        the active flights dict is empty
        """
        assert self.active_flights_handler.active_flights_num == 0

    def test_adding_new_flight(self):
        new_flight_packet = GavronTelemFrame()

        FIRST_FLIGHT_ID = int(time.time()) - 20

        new_flight_packet.header.machine_id = 1
        new_flight_packet.header.flight_id = FIRST_FLIGHT_ID
        new_flight_packet.header.timestamp = int(time.time()) - 20

        self.active_flights_handler.handle_telem_packet(new_flight_packet)

        # Check if a new flight has been created
        assert self.active_flights_handler.active_flights_num == 1

        # Check if a single packet was appended to the flight
        assert len(self.active_flights_handler.active_flights[1, FIRST_FLIGHT_ID]) == 1

        # Create a new packet for the current flight
        current_flight_new_packet = GavronTelemFrame()
        current_flight_new_packet.header.machine_id = 1
        current_flight_new_packet.header.flight_id = FIRST_FLIGHT_ID
        current_flight_new_packet.header.timestamp = int(time.time()) - 19

        self.active_flights_handler.handle_telem_packet(current_flight_new_packet)

        # Expect that the current active flight number has not changed
        assert self.active_flights_handler.active_flights_num == 1

        # Expect that a new packet was appended to the currently active flight
        assert len(self.active_flights_handler.active_flights[1, FIRST_FLIGHT_ID]) == 2

        # Create a flight from a new packet
        SECOND_FLIGHT_ID = int(time.time()) - 3

        another_flight_packet = GavronTelemFrame()
        another_flight_packet.header.machine_id = 2
        another_flight_packet.header.flight_id = SECOND_FLIGHT_ID
        another_flight_packet.header.timestamp = int(time.time()) - 3

        # Handle it
        self.active_flights_handler.handle_telem_packet(another_flight_packet)

        # There should be 2 flights now
        assert self.active_flights_handler.active_flights_num == 2

        # When checking for flight timeout, first flight should time out
        self.active_flights_handler.handle_telem_timeout()

        # There should be only 1 flight now
        assert self.active_flights_handler.active_flights_num == 1

        # And it should be the second flight
        assert self.active_flights_handler.belongs_to_any_of_currently_active_flights(
            another_flight_packet
        )

        # The first flight should not be present anymore
        assert not self.active_flights_handler.belongs_to_any_of_currently_active_flights(
            new_flight_packet
        )


if __name__ == "__main__":
    unittest.main()